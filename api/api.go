package api

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/mym4475/go-tool-kit/structgen/internal"
)

var flags = flag.NewFlagSet("gen", flag.ExitOnError)

// Generate CRUD
func Generate(cfg *Config) {
	flags.Usage = usageGenerate
	flags.Parse(os.Args[2:])

	args := flags.Args()

	//if len(args) == 0 {
	//	log.Fatal("required argument")
	//	return
	//}

	if len(args) == 0 {
		flags.Usage()
		return
	}

	tableName := args[0]

	var packageName string
	if len(args) > 1 {
		packageName = args[1]
	}

	internal.CreateAll(internal.Configuration{
		DBHost:     cfg.DB.Host,
		DBName:     cfg.DB.Name,
		DBUser:     cfg.DB.User,
		DBPassword: cfg.DB.Pass,
		TagLabel:   "db,json",
		Driver:     cfg.DB.Driver,
		Timezone:   cfg.DB.Timezone,
		DBPort:     cfg.DB.Port,
	}, tableName, packageName)
}

func usageGenerate() {
	fmt.Println(usageGenerateCommands)
}

var usageGenerateCommands = `Usage:
    gen:all {tableName}

Example:
    gen:all users`

// GenerateLogic generate logic use case
func GenerateLogic() {
	flags.Usage = usageGenerateLogic
	flags.Parse(os.Args[2:])

	args := flags.Args()

	//if len(args) == 0 {
	//	log.Fatal("required argument")
	//	return
	//}
	//
	//if len(args) < 2 {
	//	log.Fatalf("required 2 argument, example: gen:logic users user_create")
	//	return
	//}

	if len(args) < 2 {
		flags.Usage()
		return
	}

	packageName, fileName := args[0], args[1]
	internal.CreateLogic(packageName, fileName)
}

func usageGenerateLogic() {
	fmt.Println(usageGenerateLogicCommands)
}

var usageGenerateLogicCommands = `Usage:
    gen:logic {packageName} {fileName}

Example:
    gen:logic users user_create`

// GenerateEntity generate struct entity from a table
func GenerateEntity(cfg *Config) {
	flags.Usage = usageGenerateEntity
	flags.Parse(os.Args[2:])

	args := flags.Args()

	//if len(args) == 0 {
	//	log.Fatal("required argument")
	//	return
	//}

	if len(args) == 0 {
		flags.Usage()
		return
	}

	tableName := args[0]

	internal.CreateEntity(internal.Configuration{
		DBHost:     cfg.DB.Host,
		DBName:     cfg.DB.Name,
		DBUser:     cfg.DB.User,
		DBPassword: cfg.DB.Pass,
		TagLabel:   "db,json",
		Driver:     cfg.DB.Driver,
		Timezone:   cfg.DB.Timezone,
		DBPort:     cfg.DB.Port,
	}, tableName)
}

func usageGenerateEntity() {
	fmt.Println(usageGenerateEntityCommands)
}

var usageGenerateEntityCommands = `Usage:
    gen:entity {tableName}

Example:
    gen:entity users`

// GeneratePresentation generate struct presentation from a table
func GeneratePresentation(cfg *Config) {
	flags.Usage = usageGeneratePresentation
	flags.Parse(os.Args[2:])

	args := flags.Args()

	//if len(args) == 0 {
	//	log.Fatal("required argument")
	//	return
	//}

	if len(args) == 0 {
		flags.Usage()
		return
	}

	tableName := args[0]

	internal.CreatePresentation(internal.Configuration{
		DBHost:     cfg.DB.Host,
		DBName:     cfg.DB.Name,
		DBUser:     cfg.DB.User,
		DBPassword: cfg.DB.Pass,
		TagLabel:   "db,json",
		Driver:     cfg.DB.Driver,
		Timezone:   cfg.DB.Timezone,
		DBPort:     cfg.DB.Port,
	}, tableName)
}

func usageGeneratePresentation() {
	fmt.Println(usageGeneratePresentationCommands)
}

var usageGeneratePresentationCommands = `Usage:
    gen:io {tableName}

Example:
    gen:io users`

// GenerateAllEntity generate struct entities from all tables
func GenerateAllEntity(cfg *Config) {
	internal.AllTablesToEntity(internal.Configuration{
		DBHost:     cfg.DB.Host,
		DBName:     cfg.DB.Name,
		DBUser:     cfg.DB.User,
		DBPassword: cfg.DB.Pass,
		TagLabel:   "db,json",
		Driver:     cfg.DB.Driver,
		Timezone:   cfg.DB.Timezone,
		DBPort:     cfg.DB.Port,
	})
}

// GenerateAllPresentation generate struct presentations from all tables
func GenerateAllPresentation(cfg *Config) {
	internal.AllTablesToPresentation(internal.Configuration{
		DBHost:     cfg.DB.Host,
		DBName:     cfg.DB.Name,
		DBUser:     cfg.DB.User,
		DBPassword: cfg.DB.Pass,
		TagLabel:   "db,json",
		Driver:     cfg.DB.Driver,
		Timezone:   cfg.DB.Timezone,
		DBPort:     cfg.DB.Port,
	})
}

// GenerateAll CRUD generator for all tables
func GenerateAll(cfg *Config) {
	flags.Parse(os.Args[2:])
	args := flags.Args()

	var packageName string
	if len(args) > 0 {
		packageName = args[0]
	}

	internal.AllTables(internal.Configuration{
		DBHost:     cfg.DB.Host,
		DBName:     cfg.DB.Name,
		DBUser:     cfg.DB.User,
		DBPassword: cfg.DB.Pass,
		TagLabel:   "db,json",
		Driver:     cfg.DB.Driver,
		Timezone:   cfg.DB.Timezone,
		DBPort:     cfg.DB.Port,
	}, packageName)
}

package databasex

import "time"

const (
	connStringPostgresTemplate = "postgres://%s/%s?%s"
)

type (
	// Config database
	Config struct {
		Host         string
		Port         int
		User         string
		Password     string
		Name         string
		Charset      string
		MaxOpenConns int
		MaxIdleConns int
		MaxLifetime  time.Duration
		Type         string
		TimeZone     string
		Driver       string
		DialTimeout  time.Duration
		ReadTimeout  time.Duration
		WriteTimeout time.Duration
	}
)

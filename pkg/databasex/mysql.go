package databasex

import (
	"database/sql"
	"net"

	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/mym4475/go-tool-kit/structgen/pkg/util"
)

func NewMySQLSession(cfg *Config) (*sqlx.DB, error) {
	conf := mysql.NewConfig()
	conf.User = cfg.User
	conf.Passwd = cfg.Password
	conf.Passwd = cfg.Password
	conf.DBName = cfg.Name
	conf.Net = "tcp"
	conf.ParseTime = true
	conf.Timeout = cfg.DialTimeout // dial timeout
	conf.ReadTimeout = cfg.ReadTimeout
	conf.WriteTimeout = cfg.WriteTimeout
	conf.Addr = net.JoinHostPort(cfg.Host, util.ToString(cfg.Port))
	driver, err := mysql.NewConnector(conf)
	if err != nil {
		return nil, err
	}

	drv := sql.OpenDB(driver)

	drv.SetMaxOpenConns(cfg.MaxOpenConns)
	drv.SetMaxIdleConns(cfg.MaxIdleConns)
	drv.SetConnMaxLifetime(cfg.MaxLifetime)
	db := sqlx.NewDb(drv, "mysql")
	return db, nil
}

# strucgen

Struct generator covers Privy [Go-Kit](https://gitlab.com/mym4475/go-tool-kit/go-kit)

## Installation

```bash
go install
```

## Generate automatic *CRUD*

### Generate entity, presentation, repositories, use case, and DTO

Make sure already have a database and table

```bash
strucgen gen:all {TABLE_NAME}

# example
go run main.go gen:all users
```

### Generate entity only

Make sure already have a database and table

```bash
strucgen gen:entity {TABLE_NAME}

# example
strucgen gen:entity users
```

### Generate use case only

No needed database

```bash
strucgen gen:logic {PACKAGE_NAME} {FILE_NAME}

# example
strucgen gen:logic users user_create
```

### Generate presentation only

Make sure already have a database and table

```bash
strucgen gen:io {TABLE_NAME}

# example
strucgen gen:io users
```

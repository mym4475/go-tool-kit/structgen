// Package internal
package internal

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"gitlab.com/mym4475/go-tool-kit/structgen/pkg/databasex"
)

var (
	mysqlQuery = `
		SELECT
			table_name,
			column_name,
			table_schema,
			is_nullable,
			data_type,
			character_maximum_length,
			numeric_precision,
			numeric_scale,
			column_key
		FROM information_schema.columns
		WHERE table_schema = ? AND table_name = ? ORDER BY ordinal_position ASC;`

	postgresQuery = `
		SELECT
			c.table_name,
			c.column_name,
			c.table_schema,
			c.is_nullable,
			c.data_type,
			c.character_maximum_length,
			c.numeric_precision,
			c.numeric_scale,
			COALESCE(tc.constraint_type, '') AS colum_key
		FROM information_schema.table_constraints AS tc
		JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name)
		RIGHT JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema
			AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
		WHERE c.table_catalog= $1 AND c.table_name = $2 ORDER BY c.ordinal_position ASC;`

	mysqlQueryTable    = `SHOW TABLES;`
	postgresQueryTable = `SELECT table_name FROM information_schema.tables WHERE table_catalog = $1 AND table_schema = 'public';`
)

func getSchema(tableName string) []ColumnSchema {
	db, err := databasex.CreateSession(&databasex.Config{
		Driver:      config.Driver,
		Host:        config.DBHost,
		Port:        config.DBPort,
		User:        config.DBUser,
		Password:    config.DBPassword,
		TimeZone:    config.Timezone,
		Name:        config.DBName,
		DialTimeout: 30 * time.Second,
	})
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	q := mysqlQuery
	if config.Driver == "postgres" {
		q = postgresQuery
	}

	rows, err := db.Query(fmt.Sprint(q), config.DBName, tableName)
	if err != nil {
		log.Fatal(err)
	}

	columns := []ColumnSchema{}
	for rows.Next() {
		cs := ColumnSchema{}
		err := rows.Scan(
			&cs.TableName,
			&cs.ColumnName,
			&cs.TableSchema,
			&cs.IsNullable,
			&cs.DataType,
			&cs.CharacterMaximumLength,
			&cs.NumericPrecision,
			&cs.NumericScale,
			&cs.ColumnKey)
		if err != nil {
			log.Fatal(err)
		}
		columns = append(columns, cs)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	return columns
}

func listTables() []string {
	db, err := databasex.CreateSession(&databasex.Config{
		Driver:      config.Driver,
		Host:        config.DBHost,
		Port:        config.DBPort,
		User:        config.DBUser,
		Password:    config.DBPassword,
		TimeZone:    config.Timezone,
		Name:        config.DBName,
		DialTimeout: 30 * time.Second,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows := &sql.Rows{}

	if config.Driver == "mysql" {
		//rows, err = db.Query(fmt.Sprintf(mysqlQueryTable, config.DBName))
		rows, err = db.Query(mysqlQueryTable)
	}

	if config.Driver == "postgres" {
		rows, err = db.Query(postgresQueryTable, config.DBName)
	}

	if err != nil {
		log.Fatal(err)
	}

	tables := []string{}
	for rows.Next() {
		var t string
		err := rows.Scan(&t)
		if err != nil {
			log.Fatal(err)
		}
		tables = append(tables, t)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	return tables
}

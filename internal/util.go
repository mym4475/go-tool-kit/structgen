// Package internal
// @author Daud Valentino
package internal

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"os"
	"strings"

	"gitlab.com/mym4475/go-tool-kit/structgen/internal/tpl"
	"gitlab.com/mym4475/go-tool-kit/structgen/pkg/util"
)

func fileExist(fName string) bool {
	_, err := os.Stat(fName)
	return !errors.Is(err, os.ErrNotExist)
}

func contractName(v string) string {
	if util.SubStringRight(v, 1) == "e" {
		return v + "r"
	}
	return v + "er"
}

func createUseCaseList(cols []Column, packageName, tableName string) {
	tName := tableName
	if util.SubStringRight(tName, 1) == "s" {
		tName = util.SubStringLeft(tName, len(tName)-1)
	}

	pkgName := strings.ReplaceAll(packageName, "_", "")

	if len(pkgName) == 0 {
		pkgName = strings.ReplaceAll(util.ToSnakeCase(tableName), "_", "")
	}

	pkgName = fmt.Sprintf("%s/%s", ucasePath, pkgName)
	if !util.PathExist(pkgName) {
		os.MkdirAll(pkgName, os.ModePerm)
	}

	fName := fmt.Sprintf("%s/list.go", pkgName)
	if fileExist(fName) {
		log.Printf("file use case list already exist %s", fName)
		return
	}

	f, err := os.Create(fName)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	tpl, err := template.ParseFS(tpl.Templates, "ucase_list.tpl")
	if err != nil {
		log.Fatal(err)
	}

	pn := strings.Split(pkgName, "/")

	pkgNameSpace := pkgName
	if len(pn) > 1 {
		pkgNameSpace = pn[len(pn)-1]
	}

	entityName := util.UpperFirst(util.ToCamelCase(tName))
	err = tpl.Execute(f, UseCaseTemplate{
		FileName:         "list",
		TableName:        tableName,
		StructName:       util.ToCamelCase(tName),
		EntityName:       entityName,
		PackageName:      pkgNameSpace,
		ModuleName:       util.GetModuleName(),
		RepoContractName: contractName(entityName),
		Columns:          cols,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Println("success create use case list", fName)
}

func createUseCaseStorer(cols []Column, importPackage []string, packageName, tableName string) {
	tName := tableName
	if util.SubStringRight(tName, 1) == "s" {
		tName = util.SubStringLeft(tName, len(tName)-1)
	}

	//pkgName := strings.ReplaceAll(util.ToSnakeCase(tableName), "_", "")
	pkgName := strings.ReplaceAll(packageName, "_", "")
	pkgName = fmt.Sprintf("%s/%s", ucasePath, pkgName)
	if !util.PathExist(pkgName) {
		os.MkdirAll(pkgName, os.ModePerm)
	}

	fName := fmt.Sprintf("%s/store.go", pkgName)
	if fileExist(fName) {
		log.Printf("file use case storer already exist %s", fName)
		return
	}

	f, err := os.Create(fName)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	tpl, err := template.ParseFS(tpl.Templates, "ucase_store.tpl")
	if err != nil {
		log.Fatal(err)
	}

	pn := strings.Split(pkgName, "/")

	pkgNameSpace := pkgName
	if len(pn) > 1 {
		pkgNameSpace = pn[len(pn)-1]
	}

	entityName := util.UpperFirst(util.ToCamelCase(tName))
	err = tpl.Execute(f, UseCaseTemplate{
		FileName:         "store",
		TableName:        tableName,
		StructName:       util.ToCamelCase(tName),
		EntityName:       entityName,
		PackageName:      pkgNameSpace,
		RepoContractName: contractName(entityName),
		ModuleName:       util.GetModuleName(),
		Columns:          cols,
		ImportPackage:    importPackage,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Println("success create use case storer", fName)
}

func createUseCaseLogic(packageName, tableName string) {
	tName := tableName
	if util.SubStringRight(tName, 1) == "s" {
		tName = util.SubStringLeft(tName, len(tName)-1)
	}

	//pkgName := strings.ReplaceAll(util.ToSnakeCase(tableName), "_", "")
	pkgName := strings.ReplaceAll(packageName, "_", "")
	pkgName = fmt.Sprintf("%s/%s", ucasePath, pkgName)
	if !util.PathExist(pkgName) {
		os.MkdirAll(pkgName, os.ModePerm)
	}

	fileName := util.ToSnakeCase(tName)
	fName := fmt.Sprintf("%s/%s.go", pkgName, fileName)
	if fileExist(fName) {
		log.Printf("file use case already exist %s", fName)
		return
	}

	f, err := os.Create(fName)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	tpl, err := template.ParseFS(tpl.Templates, "ucase_store.tpl")
	if err != nil {
		log.Fatal(err)
	}

	pn := strings.Split(pkgName, "/")

	pkgNameSpace := pkgName
	if len(pn) > 1 {
		pkgNameSpace = pn[len(pn)-1]
	}

	entityName := util.UpperFirst(util.ToCamelCase(tName))
	err = tpl.Execute(f, UseCaseTemplate{
		FileName:         fileName,
		TableName:        tableName,
		StructName:       util.ToCamelCase(tName),
		EntityName:       util.UpperFirst(entityName),
		PackageName:      pkgNameSpace,
		RepoContractName: contractName(util.UpperFirst(entityName)),
		ModuleName:       util.GetModuleName(),
		Columns:          nil,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("success create use case %s", fName)
}

func createUseCaseUpdater(cols []Column, packageName, tableName string) {
	tName := tableName
	if util.SubStringRight(tName, 1) == "s" {
		tName = util.SubStringLeft(tName, len(tName)-1)
	}

	pkgName := strings.ReplaceAll(packageName, "_", "")
	pkgName = fmt.Sprintf("%s/%s", ucasePath, pkgName)
	if !util.PathExist(pkgName) {
		os.MkdirAll(pkgName, os.ModePerm)
	}

	fName := fmt.Sprintf("%s/update.go", pkgName)
	if fileExist(fName) {
		log.Printf("file use case updater already exist %s", fName)
		return
	}

	f, err := os.Create(fName)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	tpl, err := template.ParseFS(tpl.Templates, "ucase_update.tpl")
	if err != nil {
		log.Fatal(err)
	}

	pn := strings.Split(pkgName, "/")

	pkgNameSpace := pkgName
	if len(pn) > 1 {
		pkgNameSpace = pn[len(pn)-1]
	}

	entityName := util.UpperFirst(util.ToCamelCase(tName))
	err = tpl.Execute(f, UseCaseTemplate{
		FileName:         "update",
		TableName:        tableName,
		StructName:       util.ToCamelCase(tName),
		EntityName:       entityName,
		PackageName:      pkgNameSpace,
		RepoContractName: contractName(entityName),
		ModuleName:       util.GetModuleName(),
		Columns:          cols,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Println("success create use case updater", fName)
}

func packageName(tableName string) string {
	pkgName := strings.ReplaceAll(util.ToSnakeCase(tableName), "_", "")

	if util.SubStringRight(pkgName, 1) != "s" {
		pkgName = fmt.Sprintf("%s%s", pkgName, "s")
	}

	return pkgName
}

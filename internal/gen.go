// Package internal
// @author Daud Valentino
package internal

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/mym4475/go-tool-kit/structgen/pkg/util"
)

var (
	flags = flag.NewFlagSet("gen", flag.ExitOnError)
)

const (
	entityPath       = "internal/entity"
	repoPath         = "internal/repositories"
	presentationPath = "internal/presentations"
	ucasePath        = "internal/ucase"
	dtoPath          = "internal/dto"
)

var config Configuration

type Configuration struct {
	DBUser     string `json:"db_user"`
	DBPassword string `json:"db_password"`
	DBHost     string `json:"db_host"`
	DBPort     int    `json:"db_port"`
	DBName     string `json:"db_name"`
	// PkgName gives name of the package using the struct
	PkgName string `json:"pkg_name"`
	// TagLabel produces tags commonly used to match database field names with Go struct members
	TagLabel    string        `json:"tag_label"`
	Driver      string        `json:"driver"`
	Timezone    string        `json:"timezone"`
	DialTimeout time.Duration `json:"dial_timeout"`
}

type (
	ColumnSchema struct {
		TableName              string
		ColumnName             string
		TableSchema            string
		IsNullable             string
		DataType               string
		CharacterMaximumLength sql.NullInt64
		NumericPrecision       sql.NullInt64
		NumericScale           sql.NullInt64
		ColumnKey              string
	}

	UseCaseTemplate struct {
		TableName        string
		StructName       string
		PackageName      string
		EntityName       string
		FileName         string
		RepoContractName string
		ModuleName       string
		Columns          []Column
		ImportPackage    []string
	}
)

// formatName return formatted (camel-case) name
func formatName(name string) string {
	parts := strings.Split(name, "_")
	newName := ""
	for _, p := range parts {
		if len(p) < 1 {
			continue
		}
		newName = newName + strings.Replace(p, string(p[0]), strings.ToUpper(string(p[0])), 1)
	}

	up := map[string]string{
		"Id":  "ID",
		"Qr":  "QR",
		"Fcm": "FCM",
	}

	for k, v := range up {
		if strings.HasSuffix(newName, k) {
			newName = fmt.Sprintf("%s%s", strings.TrimSuffix(newName, k), v)
		}
	}

	return newName
}

func CreateAll(cfg Configuration, tableName, packageName string) {
	config = cfg

	columns := getSchema(tableName)
	if len(columns) == 0 {
		log.Fatalf("not found table %s", tableName)
	}

	if len(packageName) == 0 {
		packageName = fmt.Sprintf("bff/%s", tableName)
	}

	sc := generateSchema(columns, tableName)
	generatePresentation(sc)
	generateEntity(sc)
	generateRepo(sc)
	generateDTO(sc)
	createUseCaseList(sc.Column, packageName, tableName)
	createUseCaseUpdater(sc.Column, packageName, tableName)
	createUseCaseStorer(sc.Column, sc.ImportPackage, packageName, tableName)
}

func CreateEntity(cfg Configuration, tableName string) {
	config = cfg

	columns := getSchema(tableName)
	if len(columns) == 0 {
		log.Fatalf("not found table %s", tableName)
	}

	generateEntity(generateSchema(columns, tableName))
}

func CreatePresentation(cfg Configuration, tableName string) {
	config = cfg

	columns := getSchema(tableName)
	if len(columns) == 0 {
		log.Fatalf("not found table %s", tableName)
	}

	generatePresentation(generateSchema(columns, tableName))
}

func CreateLogic(packageName, fileName string) {
	createUseCaseLogic(packageName, fileName)
}

func AllTables(cfg Configuration, packageName string) {
	config = cfg

	tables := listTables()

	if len(packageName) == 0 {
		packageName = "bff"
	}

	for i := 0; i < len(tables); i++ {
		if strings.HasPrefix(tables[i], "view_") {
			continue
		}

		columns := getSchema(tables[i])
		if len(columns) == 0 {
			log.Fatalf("not found table %s", tables[i])
		}

		sc := generateSchema(columns, tables[i])
		generateEntity(sc)
		generatePresentation(sc)
		generateDTO(sc)
		generateRepo(sc)

		tableName := fmt.Sprintf("%s/%s", packageName, tables[i])
		pkgName := strings.ReplaceAll(util.ToSnakeCase(tableName), "_", "")
		if util.SubStringRight(pkgName, 1) != "s" {
			pkgName = fmt.Sprintf("%s%s", pkgName, "s")
		}

		createUseCaseList(sc.Column, fmt.Sprintf("%s/%s", packageName, tables[i]), tables[i])
		createUseCaseUpdater(sc.Column, pkgName, tables[i])
		createUseCaseStorer(sc.Column, sc.ImportPackage, pkgName, tables[i])
	}
}

func AllTablesToEntity(cfg Configuration) {
	config = cfg

	tables := listTables()

	for i := 0; i < len(tables); i++ {
		if strings.HasPrefix(tables[i], "view_") {
			continue
		}

		columns := getSchema(tables[i])
		if len(columns) == 0 {
			log.Fatalf("not found table %s", tables[i])
		}

		sc := generateSchema(columns, tables[i])
		generateEntity(sc)
	}
}

func AllTablesToPresentation(cfg Configuration) {
	config = cfg

	tables := listTables()

	for i := 0; i < len(tables); i++ {
		if strings.HasPrefix(tables[i], "view_") {
			continue
		}

		columns := getSchema(tables[i])
		if len(columns) == 0 {
			log.Fatalf("not found table %s", tables[i])
		}

		sc := generateSchema(columns, tables[i])
		generatePresentation(sc)
	}
}

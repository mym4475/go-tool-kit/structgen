package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/mym4475/go-tool-kit/structgen/api"
)

func main() {
	rootCmd := &cobra.Command{}

	cfg := api.NewConfig()

	cmds := []*cobra.Command{
		{
			Use:                "gen:all",
			Short:              "Generate CRUD",
			DisableFlagParsing: true,
			Run: func(cmd *cobra.Command, args []string) {
				api.Generate(cfg)
			},
		},
		{
			Use:                "gen:logic",
			Short:              "Generate logic use case",
			DisableFlagParsing: true,
			Run: func(cmd *cobra.Command, args []string) {
				api.GenerateLogic()
			},
		},
		{
			Use:                "gen:entity",
			Short:              "Generate struct entity from table",
			DisableFlagParsing: true,
			Run: func(cmd *cobra.Command, args []string) {
				api.GenerateEntity(cfg)
			},
		},
		{
			Use:                "gen:io",
			Short:              "Generate struct presentation from table",
			DisableFlagParsing: true,
			Run: func(cmd *cobra.Command, args []string) {
				api.GeneratePresentation(cfg)
			},
		},
		{
			Use:                "gen:all-entity",
			Short:              "Generate entities from all tables",
			DisableFlagParsing: true,
			Run: func(cmd *cobra.Command, args []string) {
				api.GenerateAllEntity(cfg)
			},
		},
		{
			Use:                "gen:all-io",
			Short:              "Generate presentations from all tables",
			DisableFlagParsing: true,
			Run: func(cmd *cobra.Command, args []string) {
				api.GenerateAllPresentation(cfg)
			},
		},
	}

	rootCmd.AddCommand(cmds...)
	rootCmd.Execute()
}
